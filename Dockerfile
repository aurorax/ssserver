FROM alpine:3.15

ENV TZ 'Asia/Shanghai'
ENV SS_LIBEV_VERSION 3.3.5
ENV SS_SIMPLE_OBFS 0.0.5
ENV LIBSODIUM_VERSION 1.0.18
ENV MBEDTLS_VERSION 2.28.0

ADD entrypoint.sh /entrypoint.sh

RUN apk upgrade --update \
    && apk add --virtual .build-deps --no-cache \
        # common dependencies
        tzdata \
        alpine-sdk \
        linux-headers \
        automake \
        autoconf \
        libtool \
        pcre-dev \
        libev-dev \
        asciidoc \
        xmlto \
        # dependencies for shadowsocks-libev
        gettext \
        c-ares-dev \
        # dependencies for simple-obfs
        libressl-dev \
        udns-dev \
    #
    && ( cp /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo $TZ > /etc/timezone ) \
    #
    && ( curl -sSLO https://download.libsodium.org/libsodium/releases/libsodium-$LIBSODIUM_VERSION.tar.gz \
    && tar -zxf libsodium-$LIBSODIUM_VERSION.tar.gz \
    && cd libsodium-$LIBSODIUM_VERSION \
    && ./configure && make \
    && make install && cd ../ ) \
    #
    && ( curl -sSL https://github.com/ARMmbed/mbedtls/archive/refs/tags/v$MBEDTLS_VERSION.tar.gz -o mbedtls-v$MBEDTLS_VERSION.tar.gz \
    && tar -zxf mbedtls-v$MBEDTLS_VERSION.tar.gz \
    && cd mbedtls-$MBEDTLS_VERSION \
    && make SHARED=1 CFLAGS=-fPIC \
    && make install && cd ../ ) \
    #
    && ( curl -sSLO https://github.com/shadowsocks/shadowsocks-libev/releases/download/v$SS_LIBEV_VERSION/shadowsocks-libev-$SS_LIBEV_VERSION.tar.gz \
    && tar -zxf shadowsocks-libev-$SS_LIBEV_VERSION.tar.gz \
    && cd shadowsocks-libev-$SS_LIBEV_VERSION \
    && ./configure && make \
    && make install && cd ../ ) \
    #
    && ( git clone https://github.com/shadowsocks/simple-obfs.git \
    && cd simple-obfs \
    && git submodule update --init --recursive \
    && ./autogen.sh \
    && ./configure && make \
    && make install && cd ../ ) \
    #
    && runDeps="$( \
        scanelf --needed --nobanner /usr/local/bin/ss-* /usr/local/bin/obfs-* \
            | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
            | xargs -r apk info --installed \
            | sort -u \
        )" \
    && apk add --virtual .run-deps $runDeps \
        privoxy \
    && echo "listen-address  0.0.0.0:8118" > /etc/privoxy/config \
    && echo "forward-socks5 / 127.0.0.1:1080 ." >> /etc/privoxy/config \
    && apk del .build-deps \
    #
    && rm -rf \
        libsodium-$LIBSODIUM_VERSION.tar.gz \
        libsodium-$LIBSODIUM_VERSION \
        mbedtls-$MBEDTLS_VERSION-gpl.tgz \
        mbedtls-$MBEDTLS_VERSION \
        shadowsocks-libev-$SS_LIBEV_VERSION.tar.gz \
        shadowsocks-libev-$SS_LIBEV_VERSION \
        simple-obfs

EXPOSE 1080
EXPOSE 8118

ENTRYPOINT ["/entrypoint.sh"]

CMD ["ss-server", "-s", "0.0.0.0", "-p", "443", "-k", "passwd", "-m", "chacha20"]
